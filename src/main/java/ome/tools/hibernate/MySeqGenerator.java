/*
 *   $Id$
 *
 *   Copyright 2006 University of Dundee. All rights reserved.
 *   Use is subject to license terms supplied in LICENSE.txt
 */
package ome.tools.hibernate;

import java.util.Properties;

import org.hibernate.MappingException;
import org.hibernate.id.PersistentIdentifierGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

/**
 * http://www.hibernate.org/296.html
 * 
 * @author josh
 * 
 */
public class MySeqGenerator extends SequenceStyleGenerator {

    /**
     * If the parameters do not contain a {@link SequenceStyleGenerator#SEQUENCE_PARAM}
     * name, we assign one based on the table name.
     */
    @Override
    public void configure(Type type, Properties params, ServiceRegistry registry)
            throws MappingException {
        if (params.getProperty(SEQUENCE_PARAM) == null
                || params.getProperty(SEQUENCE_PARAM).length() == 0) {
            String tableName = params
                    .getProperty(PersistentIdentifierGenerator.TABLE);
            if (tableName != null) {
                String seqName = "seq_" + tableName;
                params.setProperty(SEQUENCE_PARAM, seqName);
            }
        }
        super.configure(type, params, registry);
    }
}
